﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TwainScanning;
using TwainScanning.Capability;
using TwainScanning.NativeStructs;
using OnBarcode.Barcode.BarcodeScanner;
using System.Reflection;
using System.Threading;
//using Inlite.ClearImageNet;

namespace TMScanning
{
    public partial class ScanSetup : Form
    {
        bool _updating = false;
        private AcquireProcess acquireProcess;
        public static string FileLocation = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Techno-Manage-Scan\";
        int counter = 0;
        int numPages = 0;
        int pageno = 0;
        Constant c = new Constant();
        AccessInfo aa = new AccessInfo();
        FileStatus ff = new FileStatus();
        PleaseWait pleaseWait = new PleaseWait();
        public ScanSetup(AccessInfo a)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
            this.BackColor = Color.FromArgb(255, 255, 255);

            tlblFullName.Text =a.FullName;
            tlblSchoolName.Text = a.SchoolName;
            aa.FullName = a.FullName;
            aa.SchoolName = a.SchoolName;
            aa.SchoolID = a.SchoolID;
            ff.SchoolID = a.SchoolID;
            lblUser.Text = a.RoleName;
            if (a.ImgURL != "")
            {
                pictureBox1.ImageLocation = c.BaseAddress + a.ImgURL.Replace("~", "");
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            bool isValidLicense = GlobalConfig.SetLicenseKey("Knowledge Hub", "g/4JFMjn6KtGzcbjTlVGgCfMIqL00GxLDgeOgT5a3yHg7p8ZGfWXr5zUrn5E5rZk");

            //Subscribe on events
            acquireProcess = new AcquireProcess(this);
            acquireProcess.OnErrors += AcquireProcess_OnErrors;
            acquireProcess.OnImageAcquired += AcquireProcess_ImageAcquired;
            acquireProcess.OnProgressUpdate += AcquireProcess_UpdateProgress;
            acquireProcess.OnScanFinish += AcquireProcess_ScanFinish;

            lbSources.SelectedIndex = -1;
            foreach (var id in acquireProcess.Devices)
                lbSources.Items.Add(id.ProductName);
            try
            {
                tbDefSource.Text = acquireProcess.DefaultDevice.ProductName;
            }
            catch (BadRcTwainException ex)
            {
                MessageBox.Show("Bad twain return code: " + ex.ReturnCode.ToString() + "\nCondition code: " + ex.ConditionCode.ToString() + "\n" + ex.Message);
            }

            string path = FileLocation;//Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Techno-Manage-Scan\";
            System.IO.Directory.CreateDirectory(path);
            tbFileTransfer.Text = path;

            UpdateFormForCurrentDS();

         }

        private void UpdateFormForCurrentDS()
        {
            bool enableForm = acquireProcess.IsDsOpened;
            if (!enableForm)
            {
                cbDuplex.Enabled = false;
                cbADF.Enabled = false;
                cbxPixelType.Enabled = false;
                cbxResolution.Enabled = false;
                btnAsync.Enabled = false;
            }
            else
            {
                cbDuplex.Enabled = true;
                cbADF.Enabled = true;
                cbxPixelType.Enabled = true;
                cbxResolution.Enabled = true;
                btnAsync.Enabled = true;
            }
            //foreach (Control control in this.Controls)
            //{
            //    if (control == lbSources || control == btnRefresh || control == lblSources || control== lblPage || control == lblNo || control== label2 || control)
            //    {continue;}
            //    if (control == lblErrorMsg)continue;
            //    control.Enabled = enableForm;
            //}
        }

        private void lbSources_SelectedIndexChanged(object sender, EventArgs e)
        {
            string deviceName = lbSources.SelectedItem.ToString();
            acquireProcess.OpenDevice(deviceName);
            UpdateFormForCurrentDS();
            FillScannerData();
            
        }

        private void btnAsync_Click(object sender, EventArgs e)
        {
            if (!acquireProcess.IsDsOpened)
            {
                MessageBox.Show("DataSource is not opened!");
                return;
            }
            btnAsync.Enabled = false;
            if (lbTransferMode.SelectedItem.ToString() != "Memory")
            {
                try
                {
                    int index = lbTransferMode.FindString("Memory");
                    lbTransferMode.SetSelected(index, true);
                    acquireProcess.TransferMechanism = (TwSX)lbTransferMode.SelectedItem;
                    SetSelectionAndEnable(acquireProcess.TransferMechanism, lbTransferMode);
                }
                catch (Exception)
                { }
            }
            //pleaseWait.Show();
            lblScan.Show();
            lblScan.Text = "Scanning Process..";
            imageList1.Images.Clear();
            acquireProcess.Acquire(lbSources.SelectedItem.ToString(),
                null,
                null,
                tbFileTransfer.Text + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".jpg",
                cbADF.Checked,
                cbDuplex.Checked,
                (TwPixelType)cbxPixelType.SelectedItem,
                (TwSX)lbTransferMode.SelectedItem,
                (float)cbxResolution.SelectedItem,
                false,
                false,
                -1);
        }

        private void AcquireProcess_ScanFinish(CustomMultiCollector c)
        {
            try
            {
                if (this.imageList1.Images.Count > 0)
                {
                    SetPicView();
                }
                //pleaseWait.Hide();
                Thread.Sleep(1000);
                lblScan.Hide();
                btnAsync.Enabled = true;
                if (c != null)
                {
                    c.Dispose();
                }
            }
            catch (Exception ex)
            { }
        }
        
        private void AcquireProcess_ImageAcquired(Bitmap image)
        {
            Scan(image);
            //Bitmap img = image.Clone(new Rectangle(0, 0, image.Width, image.Height), image.PixelFormat);
            //pictureBox.Image = img;

        }

        //Event will be triggered only on memory transfer.
        private void AcquireProcess_UpdateProgress(int progress)
        {
            progressBar1.Enabled = true;
            progressBar1.Value = progress;
        }

        private void AcquireProcess_OnErrors(string err)
        {
            //lblErrorMsg.ForeColor = System.Drawing.Color.Red;
            //lblErrorMsg.Text = err.ToString();
            if (err.ToString().Equals("It worked!"))
                btnAsync.Enabled = true;

        }

        private void FillScannerData()
        {
            _updating = true;

            //Get all capability
            List<ICapabilityObjBase> allCaps = acquireProcess.AllCapabilities;
            Dictionary<Type, bool> tt = new Dictionary<Type, bool>();
            foreach (var c in allCaps)
                tt[c.UnderlyingType] = false;

            capability.Sorted = true;
            capability.DataSource = allCaps;
            capability.DisplayMember = "Cap";
            if (allCaps.Count < 1) { capValue.DataSource = null; }
            FillCapValueList();

            lbTransferMode.DataSource = acquireProcess.TransfersMechanisms;
            try
            {
                int index = lbTransferMode.FindString("Memory");
                lbTransferMode.SetSelected(index, true);
            }
            catch (Exception)
            {}

            cbxPixelType.DataSource = acquireProcess.PixelTypes;
            cbxResolution.DataSource = acquireProcess.Resolutions;
            comboFileTransferFormat.DataSource = acquireProcess.FileTransferFormats;

            _updating = false;

            UpdateSelections();
        }
        private void UpdateSelections()
        {

            if (!acquireProcess.IsDsOpened)
                return;

            _updating = true;

            

            acquireProcess.TransferMechanism = (TwSX)lbTransferMode.SelectedItem;
            SetSelectionAndEnable(acquireProcess.TransferMechanism, lbTransferMode);
            SetSelectionAndEnable(acquireProcess.PixelType, cbxPixelType);
            SetSelectionAndEnable(acquireProcess.Resolution, cbxResolution);
            SetSelectionAndEnable(acquireProcess.FileTransferFormat, comboFileTransferFormat);

            SetCheckedAndEnable(acquireProcess.Duplex, cbDuplex);
            SetCheckedAndEnable(acquireProcess.Feeder, cbADF);

            SetSelectionOnCapValueList();

            _updating = false;
        }
        void FillCapValueList()
        {
            ICapabilityObjBase cap = acquireProcess.CurrentCapability as ICapabilityObjBase;

            if (cap == null)
            {
                capValue.DataSource = new List<object>();
            }
            else
            {
                capValue.DataSource = new List<object>(cap.GetSupportedValuesObj());
            }
        }

        private void SetSelectionAndEnable<T>(T? prop, ComboBox c) where T : struct
        {
            c.Enabled = prop.HasValue;
            if (prop.HasValue)
                c.SelectedItem = prop.Value;
            else
                c.SelectedItem = null;
        }
        private void SetSelectionAndEnable<T>(T? prop, ListBox c) where T : struct
        {
            c.Enabled = prop.HasValue;
            if (prop.HasValue)
                c.SelectedItem = prop.Value;
            else
                c.SelectedItem = null;
        }
        private void SetCheckedAndEnable(bool? prop, CheckBox c)
        {
            c.Enabled = prop.HasValue;
            if (prop.HasValue)
                c.Checked = prop.Value;
            else
                c.Checked = false;
        }

        private void SavePath(TextBox tb)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                tb.Text = saveFileDialog1.FileName;
            }
        }

        void SetSelectionOnCapValueList()
        {
            ICapabilityObjBase cap = acquireProcess.CurrentCapability as ICapabilityObjBase;

            if (cap == null || cap.IsReadOnly)
                return;

            capValue.SelectionMode = cap.IsMultiVal ? SelectionMode.MultiExtended : SelectionMode.One;

            foreach (object o in cap.GetCurrentValuesObj())
                capValue.SelectedItems.Add(o);

        }

        private void lbTransferMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            acquireProcess.TransferMechanism = (TwSX)lbTransferMode.SelectedItem;
            UpdateSelections();
        }

        private void cbxPixelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            acquireProcess.PixelType = (TwPixelType)cbxPixelType.SelectedItem;
            UpdateSelections();
        }

        private void cbxResolution_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            acquireProcess.Resolution = (float)cbxResolution.SelectedItem;
            UpdateSelections();
        }

        private void comboFileTransferFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            acquireProcess.FileTransferFormat = (TwFF)comboFileTransferFormat.SelectedItem;
            UpdateSelections();
        }

        private void capability_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            acquireProcess.CurrentCapability = capability.SelectedItem as ICapabilityObjBase;

            _updating = true;
            FillCapValueList();
            SetSelectionOnCapValueList();
            _updating = false;
        }

        private void capValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;

            ICapabilityObjBase selCap = acquireProcess.CurrentCapability as ICapabilityObjBase;

            if (selCap == null)
                return;

            var x = new List<object>();
            foreach (object o in capValue.SelectedItems)
                x.Add(o);

            selCap.SetCurrentValuesObj(x);
            UpdateSelections();
        }


        private void ScanSetup_FormClosing(object sender, FormClosingEventArgs e)
        {
            acquireProcess.Dispose();
            Application.Exit();
        }

        private void btnFilePath_Click(object sender, EventArgs e)
        {
            SavePath(tbFileTransfer);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            lbSources.Items.Clear();
            lbSources.SelectedIndex = -1;
            foreach (var id in acquireProcess.Devices)
                lbSources.Items.Add(id.ProductName);
            try
            {
                tbDefSource.Text = acquireProcess.DefaultDevice.ProductName;
            }
            catch (BadRcTwainException ex)
            {
                MessageBox.Show("Bad twain return code: " + ex.ReturnCode.ToString() + "\nCondition code: " + ex.ConditionCode.ToString() + "\n" + ex.Message);
            }
        }

        List<string> barcodearr = new List<string>();
        delegate void ScanCallback(Bitmap objImage);
        private void Scan(Bitmap objImage)
        {
            if (this.picList.InvokeRequired)
            {
                ScanCallback d = new ScanCallback(Scan);
                this.Invoke(d, new object[] { objImage });
            }
            else
            {
                try
                {
                    string[] results = null;
                    //List<string> results = new List<string>();
                    try
                    {
                        //BarcodeReader reader = new BarcodeReader();
                        //reader.Code128 = true;
                        //reader.QR = true;
                        //reader.Pdf417 = true;
                        //Barcode[] barcodes = reader.Read(objImage);
                        //foreach (Barcode bc in barcodes)
                        //{
                        //    results.Add(bc.Text);
                        //}
                        results = BarcodeScanner.ScanSingleBarcode(objImage, BarcodeType.QRCode);
                    }
                    catch (Exception ex)
                    { }
                    if (results != null && results[0] != null && results[0] != "")
                    {
                        if (barcodearr != null && barcodearr.Count != 0 && barcodearr.Last() == results[0])
                        {
                            counter++;
                            string outputFileName = FileLocation + barcodearr.Last() + "_" + counter + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpeg";
                            objImage.Save(outputFileName);
                            SetGridView(barcodearr.Last(), counter.ToString());
                            this.imageList1.Images.Add(Image.FromFile(outputFileName));
                        }
                        else
                        {
                            counter = 1;
                            string outputFileName = FileLocation + results[0] + "_" + counter + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpeg";
                            objImage.Save(outputFileName);
                            barcodearr.Add(results[0]);
                            SetGridView(results[0], counter.ToString());
                            this.imageList1.Images.Add(Image.FromFile(outputFileName));
                        }
                    }
                    else
                    {
                        string barcode;
                        counter++;
                        if (barcodearr != null && barcodearr.Count != 0)
                        {
                            barcode = barcodearr.Last();
                        }
                        else { barcode = c.DefaultBarcode; }
                        string outputFileName = FileLocation + barcode + "_" + counter + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpeg";
                        objImage.Save(outputFileName);
                        SetGridView(barcode, counter.ToString());
                        this.imageList1.Images.Add(Image.FromFile(outputFileName));
                    }
                    // backgroundWorker1.ReportProgress(100);
                    numPages++;
                    pageno =numPages;
                }
                catch (Exception exc)
                {
                    //status = false;
                    //backgroundWorker1.CancelAsync();
                    CustomError(exc.Message);
                }
             }
        }

        delegate void SetPicCallback();
        delegate void SetGridCallback(string barcode, string fileno);
        delegate void SetPageNoCallback(int pageno);
        delegate void CustomErrorCallback(string Message);
        private void SetGridView(string barcode, string fileno)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.gridview1.InvokeRequired)
            {
                SetGridCallback d = new SetGridCallback(SetGridView);
                this.Invoke(d, new object[] { barcode, fileno });
            }
            else
            {
                bool tempAllowUserToAddRows = gridview1.AllowUserToAddRows;
                gridview1.AllowUserToAddRows = false;
                DataGridViewRow row = gridview1.Rows.Cast<DataGridViewRow>()
                .Where(r => r.Cells["Barcode"].Value.ToString().Equals(barcode)).FirstOrDefault();
                if (row==null)
                {
                    string[] data = new string[] { barcode, fileno };
                    gridview1.Rows.Add(data);
                }
                else
                {
                    row.Cells[1].Value = fileno;
                }
                int totalpage = gridview1.Rows.Cast<DataGridViewRow>()
                    .Sum(t => Convert.ToInt32(t.Cells["AttachedFile"].Value));
                SetPageNo(totalpage);
                gridview1.AllowUserToAddRows = tempAllowUserToAddRows;
            }
        }

        private void SetPicView()
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.picList.InvokeRequired)
            {
                SetPicCallback d = new SetPicCallback(SetPicView);
                this.Invoke(d, new object[] {});
            }
            else
            {
                this.picList.View = View.LargeIcon;
                this.picList.LargeImageList = this.imageList1;
                for (int j = 0; j < this.imageList1.Images.Count; j++)
                {
                    ListViewItem item = new ListViewItem();
                    item.ImageIndex =j;
                    this.picList.Items.Add(item);
                }
                
            }
        }
        private void SetPageNo(int pageno)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.lblNo.InvokeRequired)
            {
                SetPageNoCallback d = new SetPageNoCallback(SetPageNo);
                this.Invoke(d, new object[] { pageno });
            }
            else
            {
                lblNo.Show();
                lblPage.Show();
                lblNo.Text = pageno.ToString();
            }
        }

        private void CustomError(string Message)
        {
            if (this.progressBar1.InvokeRequired)
            {
                CustomErrorCallback d = new CustomErrorCallback(CustomError);
                this.Invoke(d, new object[] { Message });
            }
            else
            {
                progressBar1.Hide();
                if (Message == "Exception from HRESULT: 0x80210003")
                    MessageBox.Show("There are no documents available in the document feeder.");
                else if (Message == "Error HRESULT E_FAIL has been returned from a call to a COM component.")
                    MessageBox.Show("You need to calibrate scanner before proceed to start scan");
                else if (Message == "Exception from HRESULT: 0x80210001")
                    MessageBox.Show("An unknown error has occurred with the WIA device. Please restart scanner");
                else
                    MessageBox.Show(Message);
            }
        }

        // LogOut
        private void pictureBox4_Click_1(object sender, EventArgs e)
        {
            this.Visible = false;
            Login f1 = new Login();
            f1.Show();
        }

        // File Upload
        private void pictureBox3_Click_1(object sender, EventArgs e)
        {
            FileStatus f = new FileStatus();
            f.IsActive = true;
            f.SchoolID = ff.SchoolID;
            HMUploads f1 = new HMUploads(f);
            f1.Dgv = gridview1;
            f1.page = lblNo;
            f1.ShowDialog();
       }

        private async void ScanSetup_Load(object sender, EventArgs e)
        {
            await Task.Run(() => GridUpdate());
        }
        public void GridUpdate()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(FileLocation, "*.jpeg");
                if (filePaths != null)
                {
                    foreach (string file in filePaths)
                    {
                        if (file != null)
                        {
                            var barcode_file = (Path.GetFileName(file)).Split('_');
                            SetGridView(barcode_file[0], barcode_file[1]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {}
        }
        
        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pictureBox3, "File Upload");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pictureBox4, "Log Out");
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            acquireProcess.Dispose();
        }
    }
}
