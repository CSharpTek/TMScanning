﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TMScanning
{
    class Constant
    {
        public string BaseAddress
        {
            get
            {
                //return "http://tsn.eastus.cloudapp.azure.com";
                return "http://techno-manage.com/";
            }
        }
        public string LoginUri
        {
            get
            {
                //return "http://tsn.eastus.cloudapp.azure.com/api/WebApi/ApiAccount/Login";
                return "http://techno-manage.com/api/WebApi/ApiAccount/Login";
            }
        }
        public bool AppStatus
        {
            get
            {
                return true;
            }
        }
        public string AppRole
        {
            get
            {
                return "SchoolAdmin";
            }
        }
        public string uploadServiceBaseAddress
        {
              get
                  {
                    //return "http://tsn.eastus.cloudapp.azure.com/api/WebApi/ApiFileupload/PostData";
                    return "http://techno-manage.com/api/WebApi/ApiFileupload/PostData";
                  }
        }
        public string message { get; set; }
        public string access_token { get; set; }
        public string DefaultBarcode
        {
            get
            {
                return "0000";
            }
        }

    }
    
      public class UploadFile
       {
        public UploadFile()
        {
            ContentType = "application/octet-stream";
        }
        public string Name { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public Stream Stream { get; set; }
    }
    public class AccessInfo
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public int UserID { get; set; }
        public string RoleName { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string ImgURL { get; set; }
        public string Status { get; set; }
    }

    public class FileStatus
    {
       public bool IsActive { get; set; }
        public int SchoolID { get; set; }
    }


}
