﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace TMScanning
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            this.FormBorderStyle =FormBorderStyle.FixedSingle;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
            this.MinimumSize = new Size(436,600);
            this.MaximumSize = new Size(436, 600);
            this.BackColor= Color.FromArgb(255, 255, 255);

        }

        
        private async void btnLogin_Click_1(object sender, EventArgs e)
        {
            Constant c = new Constant();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(c.BaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/text"));

            var username = UserName.Text;
            var password = Password.Text;
            if (username == null || username == "")
                MessageBox.Show("Please Enter UserName");
            else if (password == null || password == "")
                MessageBox.Show("Please Enter Password");
            else
            {
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("UserName", username),
                    new KeyValuePair<string, string>("Password", password)
                });
                var response = await client.PostAsync(c.LoginUri, formContent);
                var stringContent = await response.Content.ReadAsStringAsync();
                JObject rootObject = JObject.Parse(stringContent);
                JObject access_info = (JObject)rootObject.GetValue("AccessInfo");
                JObject security_info = (JObject)rootObject.GetValue("SecurityInfo");
                c.message = security_info.GetValue("Message").ToString();
                c.access_token = security_info.GetValue("AccessToken").ToString();
                bool Status = Convert.ToBoolean(security_info.GetValue("Status"));
                string roleName = access_info.GetValue("RoleName").ToString();
                if (Status != c.AppStatus || roleName != c.AppRole)
                {
                    MessageBox.Show("Your UserName and Password is wrong..");
                }
                else
                {
                    AccessInfo a = new AccessInfo
                    {
                        FullName = access_info.GetValue("FullName").ToString(),
                        UserID = Convert.ToInt32(access_info.GetValue("UserID")),
                        RoleName = access_info.GetValue("RoleName").ToString(),
                        SchoolID = Convert.ToInt32(access_info.GetValue("SchoolID")),
                        SchoolName = access_info.GetValue("SchoolName").ToString(),
                        ImgURL = access_info.GetValue("ImgURL").ToString()
                    };
                    this.Hide();
                    ScanSetup f1 = new ScanSetup(a);
                    f1.Show();
                }
            }

        }

        private void Password_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click_1(this, new EventArgs());
            }
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData as string);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            linkLabel4.Links.Add(0,20, "http://techno-manage.com/");
        }
    }
}
