﻿namespace TMScanning
{
    partial class ScanSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanSetup));
            this.setCapValue = new System.Windows.Forms.Button();
            this.capValueToSet = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFileFormat = new System.Windows.Forms.Label();
            this.comboFileTransferFormat = new System.Windows.Forms.ComboBox();
            this.capValue = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.capability = new System.Windows.Forms.ListBox();
            this.btnFilePath = new System.Windows.Forms.Button();
            this.lblFileTrans = new System.Windows.Forms.Label();
            this.tbFileTransfer = new System.Windows.Forms.TextBox();
            this.cbADF = new System.Windows.Forms.CheckBox();
            this.cbDuplex = new System.Windows.Forms.CheckBox();
            this.resolutionLabel = new System.Windows.Forms.Label();
            this.pixelTypeLabel = new System.Windows.Forms.Label();
            this.cbxResolution = new System.Windows.Forms.ComboBox();
            this.cbxPixelType = new System.Windows.Forms.ComboBox();
            this.tbDefSource = new System.Windows.Forms.TextBox();
            this.lblTransMode = new System.Windows.Forms.Label();
            this.lbTransferMode = new System.Windows.Forms.ListBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnAsync = new System.Windows.Forms.Button();
            this.lblSources = new System.Windows.Forms.Label();
            this.lblDefSource = new System.Windows.Forms.Label();
            this.lbSources = new System.Windows.Forms.ListBox();
            this.gridview1 = new System.Windows.Forms.DataGridView();
            this.Barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttachedFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPage = new System.Windows.Forms.Label();
            this.lblNo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tlblFullName = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.tlblSchoolName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.picList = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblScan = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridview1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // setCapValue
            // 
            this.setCapValue.Location = new System.Drawing.Point(360, 628);
            this.setCapValue.Name = "setCapValue";
            this.setCapValue.Size = new System.Drawing.Size(36, 20);
            this.setCapValue.TabIndex = 136;
            this.setCapValue.Text = "Set";
            this.setCapValue.UseVisualStyleBackColor = true;
            this.setCapValue.Visible = false;
            // 
            // capValueToSet
            // 
            this.capValueToSet.Location = new System.Drawing.Point(191, 629);
            this.capValueToSet.Name = "capValueToSet";
            this.capValueToSet.Size = new System.Drawing.Size(157, 20);
            this.capValueToSet.TabIndex = 135;
            this.capValueToSet.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(191, 613);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 134;
            this.label1.Text = "Value to set:";
            this.label1.Visible = false;
            // 
            // lblFileFormat
            // 
            this.lblFileFormat.AutoSize = true;
            this.lblFileFormat.Location = new System.Drawing.Point(190, 499);
            this.lblFileFormat.Name = "lblFileFormat";
            this.lblFileFormat.Size = new System.Drawing.Size(58, 13);
            this.lblFileFormat.TabIndex = 132;
            this.lblFileFormat.Text = "File Format";
            this.lblFileFormat.Visible = false;
            // 
            // comboFileTransferFormat
            // 
            this.comboFileTransferFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFileTransferFormat.FormattingEnabled = true;
            this.comboFileTransferFormat.Location = new System.Drawing.Point(192, 515);
            this.comboFileTransferFormat.Name = "comboFileTransferFormat";
            this.comboFileTransferFormat.Size = new System.Drawing.Size(74, 21);
            this.comboFileTransferFormat.TabIndex = 131;
            this.comboFileTransferFormat.Visible = false;
            this.comboFileTransferFormat.SelectedIndexChanged += new System.EventHandler(this.comboFileTransferFormat_SelectedIndexChanged);
            // 
            // capValue
            // 
            this.capValue.FormattingEnabled = true;
            this.capValue.Location = new System.Drawing.Point(192, 580);
            this.capValue.Name = "capValue";
            this.capValue.Size = new System.Drawing.Size(198, 30);
            this.capValue.TabIndex = 130;
            this.capValue.Visible = false;
            this.capValue.SelectedIndexChanged += new System.EventHandler(this.capValue_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(0, 603);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 129;
            this.label7.Text = "Supported caps:";
            this.label7.Visible = false;
            // 
            // capability
            // 
            this.capability.FormattingEnabled = true;
            this.capability.Location = new System.Drawing.Point(1, 619);
            this.capability.Name = "capability";
            this.capability.Size = new System.Drawing.Size(184, 30);
            this.capability.TabIndex = 128;
            this.capability.Visible = false;
            this.capability.SelectedIndexChanged += new System.EventHandler(this.capability_SelectedIndexChanged);
            // 
            // btnFilePath
            // 
            this.btnFilePath.Location = new System.Drawing.Point(279, 552);
            this.btnFilePath.Name = "btnFilePath";
            this.btnFilePath.Size = new System.Drawing.Size(28, 22);
            this.btnFilePath.TabIndex = 126;
            this.btnFilePath.Text = "...";
            this.btnFilePath.UseVisualStyleBackColor = true;
            this.btnFilePath.Visible = false;
            this.btnFilePath.Click += new System.EventHandler(this.btnFilePath_Click);
            // 
            // lblFileTrans
            // 
            this.lblFileTrans.AutoSize = true;
            this.lblFileTrans.Location = new System.Drawing.Point(190, 538);
            this.lblFileTrans.Name = "lblFileTrans";
            this.lblFileTrans.Size = new System.Drawing.Size(84, 13);
            this.lblFileTrans.TabIndex = 124;
            this.lblFileTrans.Text = "FileTransferPath";
            this.lblFileTrans.Visible = false;
            // 
            // tbFileTransfer
            // 
            this.tbFileTransfer.Location = new System.Drawing.Point(193, 554);
            this.tbFileTransfer.Name = "tbFileTransfer";
            this.tbFileTransfer.Size = new System.Drawing.Size(80, 20);
            this.tbFileTransfer.TabIndex = 122;
            this.tbFileTransfer.Visible = false;
            // 
            // cbADF
            // 
            this.cbADF.AutoSize = true;
            this.cbADF.Location = new System.Drawing.Point(86, 201);
            this.cbADF.Name = "cbADF";
            this.cbADF.Size = new System.Drawing.Size(47, 17);
            this.cbADF.TabIndex = 120;
            this.cbADF.Text = "ADF";
            this.cbADF.UseVisualStyleBackColor = true;
            this.cbADF.Visible = false;
            // 
            // cbDuplex
            // 
            this.cbDuplex.AutoSize = true;
            this.cbDuplex.Location = new System.Drawing.Point(12, 201);
            this.cbDuplex.Name = "cbDuplex";
            this.cbDuplex.Size = new System.Drawing.Size(59, 17);
            this.cbDuplex.TabIndex = 119;
            this.cbDuplex.Text = "Duplex";
            this.cbDuplex.UseVisualStyleBackColor = true;
            // 
            // resolutionLabel
            // 
            this.resolutionLabel.AutoSize = true;
            this.resolutionLabel.Location = new System.Drawing.Point(200, 230);
            this.resolutionLabel.Name = "resolutionLabel";
            this.resolutionLabel.Size = new System.Drawing.Size(57, 13);
            this.resolutionLabel.TabIndex = 118;
            this.resolutionLabel.Text = "Resolution";
            // 
            // pixelTypeLabel
            // 
            this.pixelTypeLabel.AutoSize = true;
            this.pixelTypeLabel.Location = new System.Drawing.Point(10, 230);
            this.pixelTypeLabel.Name = "pixelTypeLabel";
            this.pixelTypeLabel.Size = new System.Drawing.Size(56, 13);
            this.pixelTypeLabel.TabIndex = 117;
            this.pixelTypeLabel.Text = "Pixel Type";
            // 
            // cbxResolution
            // 
            this.cbxResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxResolution.FormattingEnabled = true;
            this.cbxResolution.Location = new System.Drawing.Point(200, 251);
            this.cbxResolution.Name = "cbxResolution";
            this.cbxResolution.Size = new System.Drawing.Size(198, 21);
            this.cbxResolution.TabIndex = 116;
            this.cbxResolution.SelectedIndexChanged += new System.EventHandler(this.cbxResolution_SelectedIndexChanged);
            // 
            // cbxPixelType
            // 
            this.cbxPixelType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPixelType.FormattingEnabled = true;
            this.cbxPixelType.Location = new System.Drawing.Point(10, 251);
            this.cbxPixelType.Name = "cbxPixelType";
            this.cbxPixelType.Size = new System.Drawing.Size(184, 21);
            this.cbxPixelType.TabIndex = 115;
            this.cbxPixelType.SelectedIndexChanged += new System.EventHandler(this.cbxPixelType_SelectedIndexChanged);
            // 
            // tbDefSource
            // 
            this.tbDefSource.Location = new System.Drawing.Point(1, 516);
            this.tbDefSource.Name = "tbDefSource";
            this.tbDefSource.Size = new System.Drawing.Size(183, 20);
            this.tbDefSource.TabIndex = 110;
            this.tbDefSource.Visible = false;
            // 
            // lblTransMode
            // 
            this.lblTransMode.AutoSize = true;
            this.lblTransMode.Location = new System.Drawing.Point(0, 539);
            this.lblTransMode.Name = "lblTransMode";
            this.lblTransMode.Size = new System.Drawing.Size(78, 13);
            this.lblTransMode.TabIndex = 109;
            this.lblTransMode.Text = "Transfer mode:";
            this.lblTransMode.Visible = false;
            // 
            // lbTransferMode
            // 
            this.lbTransferMode.FormattingEnabled = true;
            this.lbTransferMode.Location = new System.Drawing.Point(2, 557);
            this.lbTransferMode.Name = "lbTransferMode";
            this.lbTransferMode.Size = new System.Drawing.Size(185, 43);
            this.lbTransferMode.TabIndex = 108;
            this.lbTransferMode.Visible = false;
            this.lbTransferMode.SelectedIndexChanged += new System.EventHandler(this.lbTransferMode_SelectedIndexChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(437, 636);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(564, 23);
            this.progressBar1.TabIndex = 104;
            this.progressBar1.Visible = false;
            // 
            // btnAsync
            // 
            this.btnAsync.Location = new System.Drawing.Point(9, 279);
            this.btnAsync.Name = "btnAsync";
            this.btnAsync.Size = new System.Drawing.Size(128, 30);
            this.btnAsync.TabIndex = 103;
            this.btnAsync.Text = "Start Scan";
            this.btnAsync.UseVisualStyleBackColor = true;
            this.btnAsync.Click += new System.EventHandler(this.btnAsync_Click);
            // 
            // lblSources
            // 
            this.lblSources.AutoSize = true;
            this.lblSources.Location = new System.Drawing.Point(13, 119);
            this.lblSources.Name = "lblSources";
            this.lblSources.Size = new System.Drawing.Size(63, 13);
            this.lblSources.TabIndex = 102;
            this.lblSources.Text = "All Sources:";
            // 
            // lblDefSource
            // 
            this.lblDefSource.AutoSize = true;
            this.lblDefSource.Location = new System.Drawing.Point(0, 500);
            this.lblDefSource.Name = "lblDefSource";
            this.lblDefSource.Size = new System.Drawing.Size(81, 13);
            this.lblDefSource.TabIndex = 101;
            this.lblDefSource.Text = "Default Source:";
            this.lblDefSource.Visible = false;
            // 
            // lbSources
            // 
            this.lbSources.FormattingEnabled = true;
            this.lbSources.Location = new System.Drawing.Point(12, 139);
            this.lbSources.Name = "lbSources";
            this.lbSources.Size = new System.Drawing.Size(346, 56);
            this.lbSources.TabIndex = 100;
            this.lbSources.SelectedIndexChanged += new System.EventHandler(this.lbSources_SelectedIndexChanged);
            // 
            // gridview1
            // 
            this.gridview1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridview1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.gridview1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridview1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridview1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Barcode,
            this.AttachedFile});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridview1.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridview1.Location = new System.Drawing.Point(1, 366);
            this.gridview1.Name = "gridview1";
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.gridview1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.gridview1.Size = new System.Drawing.Size(431, 234);
            this.gridview1.TabIndex = 138;
            // 
            // Barcode
            // 
            this.Barcode.HeaderText = "Barcode";
            this.Barcode.Name = "Barcode";
            this.Barcode.ReadOnly = true;
            // 
            // AttachedFile
            // 
            this.AttachedFile.HeaderText = "AttachedFile";
            this.AttachedFile.Name = "AttachedFile";
            this.AttachedFile.ReadOnly = true;
            // 
            // lblPage
            // 
            this.lblPage.AutoSize = true;
            this.lblPage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(105)))), ((int)(((byte)(35)))));
            this.lblPage.Location = new System.Drawing.Point(370, 612);
            this.lblPage.Margin = new System.Windows.Forms.Padding(50, 0, 3, 0);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(42, 15);
            this.lblPage.TabIndex = 140;
            this.lblPage.Text = "Page :";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.lblNo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblNo.Location = new System.Drawing.Point(410, 611);
            this.lblNo.Margin = new System.Windows.Forms.Padding(100, 0, 3, 0);
            this.lblNo.Name = "lblNo";
            this.lblNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblNo.Size = new System.Drawing.Size(16, 17);
            this.lblNo.TabIndex = 139;
            this.lblNo.Text = "0";
            this.lblNo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1003, 87);
            this.panel1.TabIndex = 142;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Location = new System.Drawing.Point(528, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(113, 78);
            this.panel4.TabIndex = 142;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(7, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(99, 69);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox5);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(265, 87);
            this.panel5.TabIndex = 13;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::TMScanning.Properties.Resources.technokids_logo;
            this.pictureBox5.Location = new System.Drawing.Point(11, 12);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(240, 59);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.tlblFullName);
            this.panel3.Controls.Add(this.lblUser);
            this.panel3.Controls.Add(this.tlblSchoolName);
            this.panel3.Location = new System.Drawing.Point(646, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(153, 84);
            this.panel3.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(140)))), ((int)(((byte)(105)))));
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Welcome";
            // 
            // tlblFullName
            // 
            this.tlblFullName.AutoSize = true;
            this.tlblFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblFullName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.tlblFullName.Location = new System.Drawing.Point(5, 24);
            this.tlblFullName.Name = "tlblFullName";
            this.tlblFullName.Size = new System.Drawing.Size(45, 16);
            this.tlblFullName.TabIndex = 2;
            this.tlblFullName.Text = "label2";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(103)))), ((int)(((byte)(35)))));
            this.lblUser.Location = new System.Drawing.Point(6, 44);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(45, 16);
            this.lblUser.TabIndex = 10;
            this.lblUser.Text = "label2";
            // 
            // tlblSchoolName
            // 
            this.tlblSchoolName.AutoSize = true;
            this.tlblSchoolName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblSchoolName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.tlblSchoolName.Location = new System.Drawing.Point(7, 63);
            this.tlblSchoolName.Name = "tlblSchoolName";
            this.tlblSchoolName.Size = new System.Drawing.Size(35, 13);
            this.tlblSchoolName.TabIndex = 3;
            this.tlblSchoolName.Text = "label2";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Location = new System.Drawing.Point(800, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(166, 87);
            this.panel2.TabIndex = 11;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(76, 26);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(28, 37);
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click_1);
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(120, 25);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 37);
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click_1);
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // btn_stop
            // 
            this.btn_stop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_stop.Location = new System.Drawing.Point(143, 280);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(164, 30);
            this.btn_stop.TabIndex = 143;
            this.btn_stop.Text = "Stop Scan";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh.BackgroundImage")));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRefresh.Location = new System.Drawing.Point(365, 139);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(50, 55);
            this.btnRefresh.TabIndex = 137;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // picList
            // 
            this.picList.BackColor = System.Drawing.Color.WhiteSmoke;
            this.picList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.picList.Location = new System.Drawing.Point(437, 89);
            this.picList.Name = "picList";
            this.picList.Size = new System.Drawing.Size(564, 541);
            this.picList.TabIndex = 144;
            this.picList.UseCompatibleStateImageBehavior = false;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(120, 150);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // lblScan
            // 
            this.lblScan.AutoSize = true;
            this.lblScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScan.ForeColor = System.Drawing.Color.Red;
            this.lblScan.Location = new System.Drawing.Point(11, 94);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(47, 15);
            this.lblScan.TabIndex = 145;
            this.lblScan.Text = "label3";
            this.lblScan.Visible = false;
            // 
            // ScanSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 661);
            this.Controls.Add(this.lblScan);
            this.Controls.Add(this.picList);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gridview1);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.lblNo);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.setCapValue);
            this.Controls.Add(this.capValueToSet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblFileFormat);
            this.Controls.Add(this.comboFileTransferFormat);
            this.Controls.Add(this.capValue);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.capability);
            this.Controls.Add(this.btnFilePath);
            this.Controls.Add(this.lblFileTrans);
            this.Controls.Add(this.tbFileTransfer);
            this.Controls.Add(this.cbADF);
            this.Controls.Add(this.cbDuplex);
            this.Controls.Add(this.resolutionLabel);
            this.Controls.Add(this.pixelTypeLabel);
            this.Controls.Add(this.cbxResolution);
            this.Controls.Add(this.cbxPixelType);
            this.Controls.Add(this.tbDefSource);
            this.Controls.Add(this.lblTransMode);
            this.Controls.Add(this.lbTransferMode);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnAsync);
            this.Controls.Add(this.lblSources);
            this.Controls.Add(this.lblDefSource);
            this.Controls.Add(this.lbSources);
            this.Name = "ScanSetup";
            this.Text = "ScanSetup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScanSetup_FormClosing);
            this.Load += new System.EventHandler(this.ScanSetup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridview1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button setCapValue;
        private System.Windows.Forms.TextBox capValueToSet;
        private System.Windows.Forms.Label label1;
       
        private System.Windows.Forms.Label lblFileFormat;
        private System.Windows.Forms.ComboBox comboFileTransferFormat;
        private System.Windows.Forms.ListBox capValue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox capability;
        private System.Windows.Forms.Button btnFilePath;
        private System.Windows.Forms.Label lblFileTrans;
        private System.Windows.Forms.TextBox tbFileTransfer;
        private System.Windows.Forms.CheckBox cbADF;
        private System.Windows.Forms.CheckBox cbDuplex;
        private System.Windows.Forms.Label resolutionLabel;
        private System.Windows.Forms.Label pixelTypeLabel;
        private System.Windows.Forms.ComboBox cbxResolution;
        private System.Windows.Forms.ComboBox cbxPixelType;
        private System.Windows.Forms.TextBox tbDefSource;
        private System.Windows.Forms.Label lblTransMode;
        private System.Windows.Forms.ListBox lbTransferMode;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnAsync;
        private System.Windows.Forms.Label lblSources;
        private System.Windows.Forms.Label lblDefSource;
        private System.Windows.Forms.ListBox lbSources;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridView gridview1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttachedFile;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label tlblFullName;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label tlblSchoolName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.ListView picList;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label lblScan;
    }
}